<?php
/**
 * Plugin Name: SMTP Config
 * Description: Uses an external provider to send emails from WordPress
 * Author:      Tomaz Zaman
 * Author URI:  https://codeable.io
 * Version:     1.0
 * Licence:     MIT
 */

defined( 'ABSPATH' ) or die( 'Please don\'t access this file directly.' );

/**
 * You can either hard-code values in this file or provide them
 * via environment variables.
 */

add_action('phpmailer_init', 'smtp_config');

function smtp_config($phpmailer) {
    $phpmailer->isSMTP();
    $phpmailer->Host = SMTP_HOST;
    $phpmailer->SMTPAuth = true; // Force it to use Username and Password to authenticate
    $phpmailer->Port = SMTP_PORT;
    $phpmailer->Username = SMTP_USERNAME;
    $phpmailer->Password = SMTP_PASSWORD;
    $phpmailer->SMTPSecure = SMTP_SECURE;
    $phpmailer->From = SMTP_FROM;
    $phpmailer->FromName = SMTP_FROMNAME;
}