**Começando um novo projeto**

Para começar com o template zerado, abrir a pasta "htdocs" ou equivalente no terminal e rodar a seguinte linha de comando, lembrando de mudar *NOME_DA_PASTA* para o nome do projeto.

`git clone --depth=1 --branch=master https://bitbucket.org/cappendesenvolvimento/boilerplate-wordpress.git NOME_DA_PASTA && cd NOME_DA_PASTA && rm -rf .git && git init`

Após iniciar o git na pasta do projeto, fazer o apontamento para a url do repositório:

`git remote add origin https://usuario@bitbucket.org/cappendesenvolvimento/projeto.git`

**Antes de rodar o projeto**

- Buscar **[##]** e editar o que aparecer pra ser específico do projeto. Se quiser usar search and replace:
    - [##]pasta_tema: nome da pasta de tema, sem espaços e lowercase;
    - [##]Cliente: nome do cliente / projeto, estilizado de acordo;
    - [##]Fonte: fonte principal usada no projeto.
- Ajustar os valores dos atributos `name` e `description` dos arquivos `package.json` e `composer.json`.
- Dentro da pasta do projeto, rodar bash `bin/setup`. Checar se o terminal está como bash.
- Ajustar os valores `WP_HOME`, `DB_NAME`, `DB_USER` e `DB_PASSWORD` dentro do arquivo `.env`.


**Antes de continuar o projeto**

- Editar a imagem `screnshot.png`.
- Criar a imagem `share.jpg` na pasta `/img/content` de acordo com a identidade visual do projeto, pra ser usada como imagem de compartilhamento nos sites sociais.
- Adicionar os favicons em `/img/favicons`.
- Acessar o projeto em `/web` e o Dashboard em `/web/wp/wp-admin`.


**Organização de pastas**

- CSS: `/app/scss`.
- JS: `/app/scripts`.
- Imagens: `/img/layout` para imagens usadas como elementos de layout no CSS e `/img/content` para imagens usadas como conteúdo no HTML, podendo-se criar subpastas pra páginas específicas.
- Funções do WordPress: separar em arquivos por partes específicas do projeto em `/functions` e chamar no arquivo `functions.php`.


**Finalizando o projeto**

- "Minificar" todas as imagens do projeto usando [Squoosh](https://squoosh.app) ou similar.
