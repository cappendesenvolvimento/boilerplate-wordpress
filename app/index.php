<?php get_header(); ?>

<div class="content home">
	<?php
	if (have_posts()) {
		while (have_posts()) {
			the_post();
		}
	} else {
		echo 'Nenhum conteúdo encontrado';
	}
	?>
</div>

<?php get_footer(); ?>