<?php get_header(); ?>

	<div class="content page">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="page-<?php the_ID(); ?>" class="page-content">
				<h2><?php the_title(); ?></h2>
				
				<div class="entry">
					<?php the_content(); ?>
				</div><!-- .entry -->
			</div><!-- .page-content -->
		<?php endwhile; endif; ?>
	</div><!-- .content -->

<?php get_footer(); ?>