<?php
	if(!isset($content_width)) $content_width = 1440;

	// Remove admin bar from site when user is logged in.
	function remove_admin_bar(){
		show_admin_bar(false);
	}
	add_action('after_setup_theme', 'remove_admin_bar');

	// Disable RSS
	remove_action('do_feed_rdf', 'do_feed_rdf', 10, 1);
	remove_action('do_feed_rss', 'do_feed_rss', 10, 1);
	remove_action('do_feed_rss2', 'do_feed_rss2', 10, 1);
	remove_action('do_feed_atom', 'do_feed_atom', 10, 1);

	// Remove RSS links from wp_head.
	function remove_rss_from_wp_head(){
		remove_action('wp_head', 'feed_links', 2);
		remove_action('wp_head', 'feed_links_extra', 3);
	}
	add_action('wp_head', 'remove_rss_from_wp_head', 1);

	// Remove XMLRPC
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	add_filter('xmlrpc_enabled', '__return_false');

	// Remove REST API (wp-json)
	remove_action('wp_head', 'rest_output_link_wp_head', 10);
	remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

	// Remove WP Emoji
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('admin_print_styles', 'print_emoji_styles');

	// Turn off oEmbed auto discovery.
	remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10); // Don't filter oEmbed results.
	remove_action('wp_head', 'wp_oembed_add_discovery_links'); // Remove oEmbed discovery links.
	remove_action('wp_head', 'wp_oembed_add_host_js'); // Remove oEmbed-specific JavaScript from the front-end and back-end.

	// Change the text at the of the excerpt
	function new_excerpt_more($more){
		global $post;
		return '...';
	}
	add_filter('excerpt_more', 'new_excerpt_more');
	add_filter('excerpt_length', 'my_excerpt_length');
	function my_excerpt_length($len){return 40;}

	// Remove posts and pages
	add_action('admin_menu', 'my_remove_menu_pages');
	function my_remove_menu_pages(){
		//remove_menu_page('edit.php');
		//remove_menu_page('edit.php?post_type=page');
		remove_menu_page('edit-comments.php');
	}

	// Add thumbs
	if(function_exists('add_theme_support')){
		add_theme_support('post-thumbnails');
		set_post_thumbnail_size(1060, 655, true);
		//add_image_size('##nomeaqui', 1120, 810, true);
	}

	// Get the first image from a post
	function catch_that_image(){
		global $post, $posts;
		$first_img = '';
		ob_start();
		ob_end_clean();
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
		$first_img = $matches [1] [0];
		if(empty($first_img)) $first_img = ''; //Defines a default image
		return $first_img;
	}

	// Se a busca só retornar um resultado, ir direto para o post
	function single_result(){  
		if(is_search()){  
			global $wp_query;  
			if($wp_query->post_count == 1){  
				wp_redirect(get_permalink($wp_query->posts['0']->ID));  
			}
		}
	}
	add_action('template_redirect', 'single_result');

	// Mostrar apenas posts no resultado da busca
	function searchfilter($query){
		if ($query->is_search && !is_admin()){
			$query->set('post_type',array('post'));
		}
		return $query;
	}
	add_filter('pre_get_posts','searchfilter');

	// Compressing JPEG Files to 80% quality
	// add_filter('jpeg_quality', create_function('', 'return 80;'));

	// Redirecionamentos, caso os custom post types não tenham internas
	/*function custom_redirects() {
		if(is_post_type_archive(array('banner')) || is_singular(array('banner'))){
			wp_redirect(home_url());
			exit;
		}
	}
    add_action('template_redirect', 'custom_redirects');*/
?>
