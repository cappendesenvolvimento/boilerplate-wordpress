<?php get_header(); ?>

	<div class="content inner search">
		<div class="wrapper">
			<h2>Resultados da busca</h2>

			<?php if (have_posts()) : ?>
				<h3 class="results-title success">Buscando por <span>"<?php echo esc_html(get_search_query(false)); ?>"</span></h3>

				<ul class="posts-list">
					<?php while (have_posts()) : the_post(); ?>
						<li>
							<a href="<?php the_permalink(); ?>" title="Leia mais" class="read-more">
								<h3><?php the_title(); ?></h3>
								<span class="date"><?php the_time('d'); ?>/<?php the_time('m'); ?>/<?php the_time('y'); ?></span>
								<?php
									$excerpt = get_the_excerpt();
									$hasExcerpt = substr($excerpt, 0, 350);
									$noExcerpt = substr($excerpt, 0, 280);

									if(has_excerpt()){
										echo '<p>' . $hasExcerpt . '</p>';
									} else {
										echo '<p>' . $noExcerpt . '...</p>';
									}
								?>
							</a>
						</li>
					<?php endwhile; ?>
				</ul><!-- .posts-list -->

			<?php else : ?>
				<h3 class="results-title error">Nenhum post com <span>"<?php echo esc_html(get_search_query(false)); ?>"</span> encontrado.</h3>
			<?php endif; ?>

			<div class="pagination">
				<?php
					global $wp_query;
					$big = 999999999;
					echo paginate_links(array(
						'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
						'format' => '?paged=%#%',
						'current' => max(1, get_query_var('paged')),
						'total' => $wp_query->max_num_pages,
						'prev_text' => 'Posts recentes',
						'next_text' => 'Posts antigos'
					));
				?>
			</div><!-- .pagination -->

			<?php wp_reset_postdata(); wp_reset_query(); ?>
		</div><!-- .wrapper -->
	</div><!-- .content.inner.blog -->

<?php get_footer(); ?>