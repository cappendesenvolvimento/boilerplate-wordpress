<?php get_header(); ?>

	<div class="content inner archive">
		<?php if (have_posts()) : ?>
			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

			<?php /* If this is a category archive */ if (is_category()) { ?>
				<h2>Categoria: <em><?php single_cat_title(); ?></em></h2>

			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<h2>Tag: <em><?php single_tag_title(); ?></em></h2>

			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<h2>Data: <em><?php the_time('F jS, Y'); ?></em></h2>

			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<h2>Mês: <em><?php the_time('F, Y'); ?></em></h2>

			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<h2>Ano: <em><?php the_time('Y'); ?></em></h2>

			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<h2>Posts por <em><?php the_author(); ?></em></h2>

			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<h2>Arquivos</h2>
			<?php } ?>

				<ul class="posts-list">
					<?php while (have_posts()) : the_post(); ?>
						<li>
							<a href="<?php the_permalink(); ?>" title="Leia mais" class="read-more">
								<h3><?php the_title(); ?></h3>
								<span class="date"><?php the_time('d'); ?>/<?php the_time('m'); ?>/<?php the_time('y'); ?></span>
								<?php
									$excerpt = get_the_excerpt();
									$hasExcerpt = substr($excerpt, 0, 350);
									$noExcerpt = substr($excerpt, 0, 280);

									if(has_excerpt()){
										echo '<p>' . $hasExcerpt . '</p>';
									} else {
										echo '<p>' . $noExcerpt . '...</p>';
									}
								?>
							</a>
						</li>
					<?php endwhile; ?>
				</ul><!-- .posts-list -->

			<?php else : ?>
				<h2>Arquivos</h2>
				<h3 class="results-title error">Nenhum post encontrado.</h3>
			<?php endif; ?>

		<div class="pagination">
			<?php
				global $wp_query;
				$big = 999999999;
				echo paginate_links(array(
					'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
					'format' => '?paged=%#%',
					'current' => max(1, get_query_var('paged')),
					'total' => $wp_query->max_num_pages,
					'prev_text' => 'Posts recentes',
					'next_text' => 'Posts antigos'
				));
			?>
		</div><!-- .pagination -->

		<?php wp_reset_postdata(); wp_reset_query(); ?>
	</div><!-- .content.inner.blog -->

<?php get_footer(); ?>