<?php get_header(); ?>

	<div class="content inner single">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" class="post">
				<div class="info">
					<div class="meta">
						<h3><?php the_title(); ?></h3>

						<?php $category = get_the_category(); ?>
						<span class="category"><a href="<?php echo get_category_link($category[0]->term_id); ?>" title="Ver todos os posts da categoria <?php echo $category[0]->cat_name; ?>"><?php echo $category[0]->cat_name; ?></a></span>
						
						<span class="date"><?php the_time('d'); ?>/<?php the_time('m'); ?>/<?php the_time('y'); ?></span>
					</div><!-- .meta -->

					<?php if(has_post_thumbnail()){
						echo '<div class="thumb">' . get_the_post_thumbnail() . '</div><!-- .thumb -->';
					} ?>
				</div><!-- .info -->

				<div class="entry">
					<?php the_content(); ?>

					<div class="share">
						<div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>

						<a href="https://twitter.com/share" class="twitter-share-button"{count} data-url="<?php the_permalink(); ?>" data-lang="pt">Tweetar</a>
						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					</div><!-- .share -->
				</div><!-- .entry -->

				<div class="comments">
					<h3>Comentários</h3>
					
					<?php /*Caso seja do facebook: <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>*/ ?>
				</div><!-- .comments -->
			</div><!-- .post -->

			<div class="post-navigation">
				<?php next_post_link('%link'); ?>
				<?php previous_post_link('%link'); ?>
			</div><!-- .post-navegation -->
		<?php endwhile; endif; ?>
	</div><!-- .content.inner.single -->

<?php get_footer(); ?>