<?php get_header(); ?>

	<div id="error-404" class="content">
		<div class="container">
			<h2>Página não encontrada</h2>
			<p>Por favor, tente novamente.</p>
		</div><!-- .container -->
	</div><!-- .content -->

<?php get_footer(); ?>