**Requerimentos:**

- PHP 7.1 ou superior
- MySQL 5.7 ou superior
- NodeJS (https://nodejs.org/)
- Composer (https://getcomposer.org/)


**Instruções para a primeira configuração de um projeto:**

Consultar o arquivo `CONFIG.md`.


**Para modificar qualquer comportamento no front-end, seguir os sequintes passos:**

-   Abrir o terminal e ir para a raiz do projeto;
-   Executar `npm install` para instalar todas as dependências;
-   Quando a instalação finalizar, executar `npm run dev` para compilar; Isto irá rodar o `watch` em modo de desenvolvimento; Para produção, executar `npm run build`.
